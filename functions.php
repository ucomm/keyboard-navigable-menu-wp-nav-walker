<?php
// Wordpress runs all the main theme functions from this file.

require_once(__DIR__ . '/classes/class-nav-test-walker.php');

// enqueue css and js for the nav menu
function nav_walker_test_enqueue_scripts() {
  wp_enqueue_style('nav-walker-styles', get_stylesheet_directory_uri() . '/build/main.css');
  wp_enqueue_script('nav-walker-script', get_stylesheet_directory_uri() . '/build/index.js', array('jquery'), false, true);
}

add_action('wp_enqueue_scripts', 'nav_walker_test_enqueue_scripts');




