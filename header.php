<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php do_action('fl_head_open'); ?>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php FLTheme::title(); ?>
<?php FLTheme::favicon(); ?>
<?php FLTheme::fonts(); ?>
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
<![endif]-->
<?php 

wp_head(); 

FLTheme::head();

?>
</head>

<body <?php body_class(); ?> itemscope="itemscope" itemtype="http://schema.org/WebPage">
<?php 
	
FLTheme::header_code();
	
do_action('fl_body_open'); 

?>
<div class="fl-page">
	<?php
	
	do_action('fl_page_open');
	
	FLTheme::fixed_header();
	
	do_action('fl_before_top_bar');
	
	FLTheme::top_bar();
	
	do_action('fl_after_top_bar');
	do_action('fl_before_header');
	
  // FLTheme::header_layout();
  ?>
  <header class="fl-page-header fl-page-header-primary<?php FLTheme::header_classes(); ?>"<?php FLTheme::header_data_attrs(); ?> itemscope="itemscope" itemtype="http://schema.org/WPHeader">
	<div class="fl-page-header-wrap">
		<div class="fl-page-header-container container">
			<div class="fl-page-header-row row">
				<div class="col-md-6 col-sm-6 fl-page-header-logo-col">
					<div class="fl-page-header-logo" itemscope="itemscope" itemtype="http://schema.org/Organization">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url"><?php FLTheme::logo(); ?></a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="fl-page-header-content">
						<?php FLTheme::header_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="fl-page-nav-wrap">
		<div class="fl-page-nav-container container">
			<nav class="fl-page-nav navbar navbar-default" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".fl-page-nav-collapse">
					<span><?php FLTheme::nav_toggle_text(); ?></span>
				</button>

				<!-- Begin keyboard accessible nav menu -->

				<!-- label the menu for screen readers. -->
				<div aria-label="main menu" class="fl-page-nav-collapse collapse navbar-collapse">
					<?php
					wp_nav_menu(array(
						'theme_location' => 'header',
						// give the list a role of menubar to make it an ARIA landmark
						'items_wrap' => '<ul id="%1$s" class="nav navbar-nav %2$s">%3$s</ul>',
						'container' => false,
            'fallback_cb' => 'FLTheme::nav_menu_fallback',
            'walker' => new Nav_Test_Walker()
					));

					FLTheme::nav_search();

					?>
				</div>

				<!-- End keyboard accessible nav menu -->



			</nav>
		</div>
	</div>
</header><!-- .fl-page-header -->
  <?php	
	do_action('fl_after_header');
	do_action('fl_before_content');
	
	?>
	<div class="fl-page-content" itemprop="mainContentOfPage">
	
		<?php do_action('fl_content_open'); ?>